FROM openjdk:8-jdk-alpine

VOLUME /tmp
ADD /target/personnel.jar app.jar
ADD /firebaseAccount.json firebaseAccount.json

EXPOSE 8080

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=prod","-jar","/app.jar"]