# Micro-service de gestion du personnel

[![pipeline status](https://gitlab.com/uf-web/backend/personnel/badges/master/pipeline.svg)](https://gitlab.com/uf-web/backend/personnel/-/commits/master)

## Description

Ce micro-service est destiné à la gestion du personnel.

## Information d'une personne

Chaque personne possède les informations suivantes :
* Identifiant
* Nom d'utilisateur
* Mot de passe
* Civilité (M, Mme, Autre)
* Prénom
* Nom
* Date de naissance
* Variable est actif ?
* Email