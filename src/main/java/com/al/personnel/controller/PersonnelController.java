package com.al.personnel.controller;

import com.al.personnel.model.Personnel;
import com.al.personnel.service.FirebaseService;
import com.al.personnel.service.PersonnelService;
import com.sipios.springsearch.anotation.SearchSpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * Controller du micro-service Personnel
 *
 * @author Antoine Thys
 */

@RestController
@RequestMapping(value = "/personnel")
public class PersonnelController {

    @Autowired
    @Resource
    PersonnelService personnelService;

    @Autowired
    @Resource
    FirebaseService firebaseService;

    /**
     * Récupération de tous les Personnels avec pagination
     *
     * @param pageable Argument pour pagination
     * @return List de tous les Personnels avec pagination
     */
    @GetMapping()
    public Page<Personnel> getAllPersonnel(Pageable pageable) {
        return personnelService.getAllPersonnel(pageable);

    }

    /**
     * Récupération d’un Personnel
     *
     * @param id Identifiant d’un Personnel
     * @return Personnel spécifié
     */
    @GetMapping(value = "/{id}")
    public Optional<Personnel> getPersonnelById(@PathVariable(value = "id") Integer id) {
        return personnelService.getPersonnelById(id);
    }

    /**
     * Recherche de Personnels
     *
     * @param specs    Spécifications de recherche (https://github.com/sipios/spring-search#Usage)
     * @param pageable Pagination
     * @return Page de Personnel + Http Status OK
     */
    @GetMapping(value = "/search")
    public ResponseEntity<Page<Personnel>> searchPersonnel(@SearchSpec Specification<Personnel> specs, Pageable pageable) {
        return new ResponseEntity<>(personnelService.searchPersonnels(specs, pageable), HttpStatus.OK);
    }

    /**
     * Création de Personnel
     *
     * @param personnel Personnel a créé
     * @return Personnel créé && Status Http "CREATED"
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Optional<Personnel>> createPersonnel(@RequestBody Personnel personnel) {
        System.out.println(personnel);
        firebaseService.createUser(personnel);
        Optional<Personnel> c = personnelService.createPersonnel(personnel);
        return new ResponseEntity<>(c, HttpStatus.CREATED);
    }

    /**
     * Mise à jour d’un Personnel
     *
     * @param personnel Nouvelles informations du Personnel
     * @param id        ID du Personnel à modifier
     * @return Personnel modifié && Status Http "OF"
     */
    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Personnel> updatePersonnelPut(@RequestBody Personnel personnel, @PathVariable("id") Integer id) {
        personnel.setId(id);
        Personnel u = personnelService.updatePersonnelPut(personnel);
        return new ResponseEntity<>(u, HttpStatus.OK);
    }

    /**
     * Suppression d’un Personnel
     *
     * @param id ID du Personnel à supprimer
     * @return Status Http "OK"
     */
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<HttpStatus> deletePersonnel(@PathVariable("id") Integer id) {
        firebaseService.deleteUser(personnelService.getPersonnelById(id));
        personnelService.deletePersonnel(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
