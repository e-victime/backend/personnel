package com.al.personnel.model;

import com.al.personnel.model.bilan.Physique;
import com.al.personnel.model.bilan.Psy;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "victime")
@FieldDefaults(level= AccessLevel.PRIVATE)
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode
public class Victime {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;

    Integer numeroVictime;

    @CreationTimestamp
    Timestamp timestamp;

    String identifiant;

    @NonNull
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm", iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    LocalDateTime arrive;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm", iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    LocalDateTime depart;

    LocalDate dateNaissance;

    Integer age;

    @NonNull
    @Enumerated(EnumType.STRING)
    Genre sexe;

    @NonNull
    String nationalite;

    String adresse;

    String numero;

    String personnePrevenir;

    @Column(columnDefinition = "TEXT")
    String commentaire;

    Boolean dv;
    Boolean dcd;
    Boolean trauma;
    Boolean vetement;
    Boolean assistance;
    Boolean repo;
    Boolean transport;
    Boolean soin;
    Boolean malaise;
    Boolean refusPA;
    Boolean medicalisee;
    Boolean alimentation;
    Boolean impliquee;
    Boolean repartiPropreMoyen;

    @JsonManagedReference(value = "physique")
    @OneToOne(mappedBy = "victime", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    Physique bilanPhysique;

    @JsonManagedReference(value = "psy")
    @OneToOne(mappedBy = "victime", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    Psy bilanPsy;

    @JsonBackReference(value = "InterventionsForEquipe")
    @ManyToOne(targetEntity = Equipe.class)
    @JoinColumn(name="equipe_id")
    Equipe equipe;

    @JsonBackReference(value = "VictimesForPoste")
    @ManyToOne(targetEntity = Poste.class)
    @JoinColumn(name="poste_id")
    Poste poste;

}
