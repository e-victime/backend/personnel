package com.al.personnel.model;

import com.fasterxml.jackson.annotation.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "poste")
@FieldDefaults(level= AccessLevel.PRIVATE)
@Getter
@Setter
@ToString(of = {
        "nom",
        "dateDebut",
        "dateFin",
        "descriptionPoste"
})
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Poste {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;

    @NonNull
    @Column(name = "nom", nullable = false)
    String nom;

    @Column(name = "date_debut")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Europe/Paris")
    LocalDateTime dateDebut;

    @Column(name = "date_fin")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Europe/Paris")
    LocalDateTime dateFin;

    @Column(name = "location")
    String location;

    @Column(name = "description")
    String descriptionPoste;

    @OneToMany(mappedBy = "poste", cascade = CascadeType.ALL)
    Set<Equipe> equipes;

    @ManyToOne
    @JoinColumn
    Personnel responsable;

    @JsonManagedReference(value = "VictimesForPoste")
    @OneToMany(mappedBy = "poste")
    Set<Victime> victimes;

    @ElementCollection
    List<Long> victimesIds = new ArrayList<>();
}
