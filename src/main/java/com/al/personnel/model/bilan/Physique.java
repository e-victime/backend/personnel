package com.al.personnel.model.bilan;

import com.al.personnel.model.Victime;
import com.al.personnel.model.bilan.Glasgow.Motricite;
import com.al.personnel.model.bilan.Glasgow.Verbale;
import com.al.personnel.model.bilan.Glasgow.Yeux;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalTime;
import java.util.Set;

/**
 * Classe pour le Bilan Physique d’une Victime
 * Chaque variable est préfixé du type de Bilan
 * S’il s’agit d’un acte médical la variable est suffixé "med"
 * Attention aux variables de type Enum
 */
@Entity
@Table(name = "bilan_physique")
@FieldDefaults(level= AccessLevel.PRIVATE)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@TypeDef(
        name = "pgsql_enum",
        typeClass = PostgreSQLEnumType.class
)
public class Physique {
    @Id
    @Column(name = "id")
    private Long id;

    // Bilan Circonstanciel
    @Enumerated(EnumType.STRING)
    @Type( type = "pgsql_enum" )
    LieuInter circonstanciel_lieuIntervention;
    String circonstanciel_commentaireLieuIntervention;
    @Enumerated(EnumType.STRING)
    @Type( type = "pgsql_enum" )
    NatureInter circonstanciel_natureIntervention;
    @Enumerated(EnumType.STRING)
    @Type( type = "pgsql_enum" )
    TypeIntox circonstanciel_typeIntoxication;
    String circonstanciel_detailIntoxication;
    String circonstanciel_commentaire;

    // Demande de renforts
    String renforts_demandes, renforts_risques, renforts_commentaire;

    // Bilan Neurologique
    Boolean neuro_conscience;
    @Column(columnDefinition = "integer default 0")
    Integer neuro_PCI;
    Boolean neuro_desorientation, neuro_agitation, neuro_vomissements, neuro_convulsions, neuro_pupillesReact_med;
    @Enumerated(EnumType.STRING)
    @Type( type = "pgsql_enum" )
    PupillesSym neuro_symetriePupilles;
    @Enumerated(EnumType.STRING)
    @Type( type = "pgsql_enum" )
    TroubleMoteur neuro_troublesMoteurs;

    // Glasgow (Médical)
    @Enumerated
    Yeux glasgow_ouvertureYeux_med;
    @Enumerated
    Motricite glasgow_reponseMotrice_med;
    @Enumerated
    Verbale glasgow_reponseVerbale_med;
    @Nullable
    Integer glasgow_score;

    // Bilan Respiratoire
    Boolean respiratoire_ventilation, respiratoire_facile, respiratoire_reguliere, respiratoire_pauses, respiratoire_cyanoses, respiratoire_ample, respiratoire_bruyante, respiratoire_sueurs;
    Integer respiratoire_frequence;
    @Min(0) @Max(100)
    Integer respiratoire_spo2;

    // Bilan Circulatoire
    Boolean circulatoire_poulsCarotidien, circulatoire_poulsRadial, circulatoire_poulsBF, circulatoire_poulsRegulie,circulatoire_paleur;
    Integer circulatoire_frequence, circulatoire_pressionSys, circulatoire_pressionDias;
    String circulatoire_TRC;
    EtatPeau circulatoire_etatPeau;

    // Bilan Complémentaire
    @Enumerated(EnumType.STRING)
    @Type( type = "pgsql_enum" )
    TypeTrauma complementaire_traumaPrincpal;
    @Enumerated(EnumType.STRING)
    @Type( type = "pgsql_enum" )
    Localisation complementaire_localisationTrauma;
    @Enumerated(EnumType.STRING)
    @Type( type = "pgsql_enum" )
    CoteTrauma complementaire_coteTrauma;
    @Min(0) @Max(4)
    Integer complementaire_douleur;
    String complementaire_description;

    // Gestes
    Boolean gestes_collier, gestes_attelle, gestes_MID, gestes_PLS, gestes_allonge, gestes_assis, gestes_demiAssis, gestes_retraitCasque, gestes_desinfection, gestes_froid, gestes_CHU, gestes_garrot, gestes_aspi, gestes_O2, gestes_BAVU, gestes_canule, gestes_RCP, gestes_DAE, gestes_aideMed;
    String gestes_autreImmobilisation, gestes_autreGeste, gestes_typeMed, gestes_glycemie;
    Integer gestes_O2L, gestes_DAEChocs;
    Float gestes_temp;
    LocalTime gestes_heureGarrot, gestes_heureDAE;

    // Présence sur les lieux
    Boolean presence_medecin, presence_SAMU, presence_police, presence_infirmier, presence_SP;

    // Devenir de la victime
    @Enumerated(EnumType.STRING)
    @Type( type = "pgsql_enum" )
    EvacType devenir_evacPar;
    @Enumerated(EnumType.STRING)
    @Type( type = "pgsql_enum" )
    EvacDest devenir_evacDestType;
    LocalTime devenir_arriveeTransport;
    String devenir_evacDestination;
    Boolean devenir_laisseSurPlace, devenir_refusPA, devenir_DCD_med;

    // Heure de passage de bilan
    LocalTime transmission_pc, transmission_SAMU;
    String transmission_observations;

    @JsonManagedReference(value = "surveillance")
    @OneToMany(mappedBy = "bilan", cascade = CascadeType.ALL)
    Set<Surveillance> surveillance;


    // Relations
    @JsonBackReference(value = "physique")
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @MapsId
    Victime victime;

    public Physique(Victime victime) {
        this.victime = victime;
    }
}
