package com.al.personnel.model.bilan;

/**
 * Evolution de l’état psy
 */
public enum EvolutionPsy {
    SansChangement, Amelioration, Aggravation
}
