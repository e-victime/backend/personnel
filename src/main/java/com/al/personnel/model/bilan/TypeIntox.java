package com.al.personnel.model.bilan;

public enum TypeIntox {
    Alcool, CO, Medicament, Drogue, Alimentaire
}
