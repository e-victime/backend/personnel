package com.al.personnel.model.bilan;

public enum NatureInter {
    Accident, Maladie, Malaise, Intoxication, Noyade, Autre
}
