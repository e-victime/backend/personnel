package com.al.personnel.model.bilan;

public enum Recit {
    /**
     * FacE : Factuel Exclusif
     * EmoE : Émotionnel Exclusif
     * FacEmo : Factuel et Émotionnel
     * Amne : Amnésie
     */
    FacE, EmoE, FacEmo, Amne
}
