package com.al.personnel.model.bilan;

/**
 * Contact Relationnel
 */
public enum Relationnel {
    /**
     * S : Satisfaisant
     * PS : Peu satisfaisant
     *  IS : Insatisfaisant
     */
    S, PS , IS
}
