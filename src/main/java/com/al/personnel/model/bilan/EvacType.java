package com.al.personnel.model.bilan;

public enum EvacType {
    PROTEC, SP, SMUR, PRIVE, AUTRE, HELICO
}
