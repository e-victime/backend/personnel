package com.al.personnel.model.bilan;

public enum CoteTrauma {
    Droit, Gauche, Deux, Avant, Arriere
}
