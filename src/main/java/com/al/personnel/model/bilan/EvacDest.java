package com.al.personnel.model.bilan;

public enum EvacDest {
    /**
     * CH : Centre
     * PMA : Poste Médical Avancé
     * IPR : Infirmerie Principale
     * Accueil : Accueil de jour/nuit
     * Cabinet : Cabinet médical
     * Douche : Douche publique
     * MissionLocale : Mission Locale
     *
     */
    Autre, CH, Clinique, PMA, IPR, Accueil, Cabinet, Douche, MissionLocale
}
