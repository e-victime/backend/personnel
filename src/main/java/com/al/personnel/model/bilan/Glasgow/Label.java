package com.al.personnel.model.bilan.Glasgow;

public interface Label {
    String label();
    Integer score();
}
