package com.al.personnel.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Table(name = "members")
@FieldDefaults(level= AccessLevel.PRIVATE)
@Getter
@Setter
@ToString(of = {
    "role"
})
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode
public class Members {

    @EmbeddedId @NonNull
    MembersKey id;

    @NonNull
    @ManyToOne @MapsId("equipeId")
    @JoinColumn(name = "equipe_id")
    Equipe equipe;

    @NonNull
    @ManyToOne @MapsId("personnelId")
    @JoinColumn(name = "personnel_id")
    Personnel personnel;

    @Column(name = "role")
    String role;
}
