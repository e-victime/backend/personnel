package com.al.personnel.service;

import com.al.personnel.model.Personnel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FirebaseService {

//    Personnel{username='ivoireoui', prenom='Louis', nom='Lacoste', dateNaissance=2000-07-06, actif=true, email='Louislacoste33@gmail.com'}

    public void createUser(Personnel personnel) {
        UserRecord.CreateRequest request = new UserRecord.CreateRequest()
                .setEmail(personnel.getEmail())
                .setEmailVerified(false)
                .setPassword(personnel.getPassword())
                .setPhoneNumber(personnel.getPhone())
                .setDisplayName(personnel.getPrenom() + " " + personnel.getNom())
                .setPhotoUrl("https://www.dovercourt.org/wp-content/uploads/2019/11/610-6104451_image-placeholder-png-user-profile-placeholder-image-png.jpg")
                .setDisabled(false);

        UserRecord userRecord;
        try {
            userRecord = FirebaseAuth.getInstance().createUser(request);
            System.out.println("Successfully created new user: " + userRecord.getUid());
        } catch (FirebaseAuthException e) {
            System.out.println("---------- ERROR WHILE CREATING USER IN FIREBASE -----------");
            e.printStackTrace();
        }
    }

    public void deleteUser(Optional<Personnel> personnel) {

        String uid = null;

        try {
            uid = FirebaseAuth.getInstance().getUserByEmail(personnel.get().getEmail()).getUid();
        } catch (FirebaseAuthException e) {
            e.printStackTrace();
        }

        try {
            FirebaseAuth.getInstance().deleteUser(uid);
        } catch (FirebaseAuthException e) {
            System.out.println("---------- COULDN'T DELETE USER ----------");
            e.printStackTrace();
        }

    }

}
