package com.al.personnel.service;

import com.al.personnel.model.Personnel;
import com.al.personnel.repository.PersonnelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

@Service
public class PersonnelServiceImpl implements PersonnelService{

    @Autowired
    @Resource
    PersonnelRepository personnelRepository;

    /**
     * @param pageable Argument de pagination
     * @return List de tous les personnels avec pagination
     */
    @Override
    public Page<Personnel> getAllPersonnel(Pageable pageable) {
        return personnelRepository.findAll(pageable);
    }

    /**
     * Trouver un personnel avec son ID
     * @param personnelId ID d’un Personnel
     * @return Personnel
     */
    @Override
    public Optional<Personnel> getPersonnelById(Integer personnelId) {
        if (!personnelRepository.existsById(personnelId)) {
            throw new ResourceNotFoundException("Personnel avec l'ID " + personnelId + " non trouvé.");
        }
        return personnelRepository.findById(personnelId);
    }

    /**
     * Création d’un Personnel
     * @param personnel Personnel
     * @return Personnel créé
     */
    @Override
    public Optional<Personnel> createPersonnel(Personnel personnel) {
        Integer p = personnelRepository.save(personnel).getId();
        return personnelRepository.findById(p);
    }

    /**
     * Trouver un personnel par nom d’utilisateur
     * @param username Nom d’utilisateur de l’utilisateur à chercher
     * @return Le Personnel avec le nom d’utilisateur en paramètre
     */
    @Override
    public Optional<Personnel> getPersonnelByUsername(String username) {
        try {
            return personnelRepository.findPersonnelByUsername(username);
        } catch (Exception exc) {
            System.err.println(exc.getMessage());
        }
        return Optional.empty();
    }

//    @Override
//    public Personnel updatePersonnelPatch(Personnel personnel, Integer id) {
//        if (personnelRepository.findById(id).isPresent()) {
//            return personnelRepository.save(personnel);
//        }
//        return null;
//    }

    /**
     * Mise à jour d’un personnel
     * @param personnel Personnel à mettre à jour
     * @return Personnel avec mis à jour
     */
    @Override
    public Personnel updatePersonnelPut(Personnel personnel) {
        return personnelRepository.save(personnel);
    }

    /**
     * Suppression d’un Personnel
     * @param id Personnel à supprimer
     */
    @Override
    public void deletePersonnel(Integer id) {
        if (personnelRepository.findById(id).isPresent()) {
            Personnel delete = personnelRepository.findById(id).get();
            personnelRepository.delete(delete);
        }
    }

    /**
     * Recherche d’un utilisateur avec spécifications
     * @param specs Spécifications
     * @param pageable Pagination
     * @return Page de Personnel
     */
    @Override
    public Page<Personnel> searchPersonnels(Specification<Personnel> specs, Pageable pageable) {
        return personnelRepository.findAll(Specification.where(specs),pageable);
    }

}
