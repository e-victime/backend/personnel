package com.al.personnel.service;

import com.al.personnel.model.Personnel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface PersonnelService {
    Page<Personnel> getAllPersonnel(Pageable pageable);
    Optional<Personnel> getPersonnelById(Integer personnelId);
    Optional<Personnel> createPersonnel(Personnel personnel);
    Optional<Personnel> getPersonnelByUsername(String username);
//    Personnel updatePersonnelPatch(Personnel personnel, Integer id);
    Personnel updatePersonnelPut(Personnel personnel);
    void deletePersonnel(Integer id);
    Page<Personnel> searchPersonnels(Specification<Personnel> specs, Pageable pageable);

}
