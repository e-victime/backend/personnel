package com.al.personnel.repository;

import com.al.personnel.model.Personnel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

//@RepositoryRestResource
@Repository
public interface PersonnelRepository extends JpaRepository<Personnel, Integer>, JpaSpecificationExecutor<Personnel> {

//    @Query("SELECT p FROM Personnel p WHERE p.username = '1'")
    Optional<Personnel> findPersonnelByUsername(String username);
}
