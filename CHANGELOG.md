# CHANGELOG

<!--- next entry here -->

## 1.2.1
2020-05-26

### Fixes

- **validation:** Add validator on email and phone number (4311c4bf68c583814436f49e40275b04f2985968)

## 1.2.0
2020-05-26

### Features

- **personnel:** Add phone number for firebase compatibility (12aafd85663e28f61875109ce9366b90083f9276)

### Fixes

- **firebase:** Delete phone number (ed7e78ebbdee1ed3dce7571803757662b25f35bb)
- **firebase:** Update firebase (0b3dfc90665565101379ab43713e9365c18b0eb7)

## 1.1.8
2020-05-26

### Fixes

- **db:** increase timeout time (451095cefbcfd23778f424e15ec754c3a23cfb04)

## 1.1.7
2020-05-26

### Fixes

- **discovery:** Change zone URL (b4119570c776df0c80ab03a0b4c5ad79310b7146)

## 1.1.6
2020-05-25

### Fixes

- **discovery:** Fix missing port on Eureka URL (ba8f4eedcec347a6d581ffdb647a5d075fb203bb)

## 1.1.5
2020-05-25

### Fixes

- **env vars:** Change Eureka env var (6341c218034b0d8531f12ba6de2939eb6ed97dc3)

## 1.1.4
2020-05-19

### Fixes

- **versioning:** Repair versioning in POM (6e9355662ebab4eb2d46baea09aa267f0589ca6b)

## 1.1.3
2020-05-18

### Fixes

- **cors:** CORS for swagger ui (2c69b60d8b14542277ae90a4be97c84dda081390)

## 1.1.2
2020-05-18

### Fixes

- **cors:** CORS for swagger ui (e2ab1691074fcc068d6fbe1cc95972b3efa30313)
- **cors:** CORS for swagger ui (7f067709c7b15cfb5d10438102c30f1072bc05ac)

## 1.1.1
2020-05-11

### Fixes

- **monitoring:** Expose all actuator routes (69a1d4d99fa9b0168550c4ba89cfdef38fb08c93)

## 1.1.0
2020-05-11

### Features

- **cloud:** Add discovery Client (3a807a5467358ce56c3f49576bc785e35d9c51e8)

## 1.0.5
2020-05-09

### Fixes

- **pom:** Update dependencies (092d42573e9e35aa8b8eb0026409b97cc1accc3c)

## 1.0.4
2020-05-09

### Fixes

- **deployment:** Change registry url (fc6767d72c669f8f6cbcd77aca8be34e01ffba87)

## 1.0.3
2020-05-09

### Fixes

- **config:** Add discovery config (e4875073779a056a030a74b4cdabbdc9d65e06c3)

## 1.0.2
2020-05-09

### Fixes

- **var:** Update env variables (dab7c13a6a8e66a3d269909605ea489a43d7fa74)

## 1.0.1
2020-05-08

### Fixes

- **spring boot:** Update Spring Boot (f0a51a36c65a847d0c42d0f1668861e067138f03)

## 1.0.0
2020-05-03

### Fixes

- **ci:** Add versionning in pom file (b94255bb8688ab8d981d2fca6bf294e4d2fb901b)

## 0.14.2
2020-04-30

### Fixes

- **pom:** Update pom (91a8cb324944464aaf1ef41b33f437c5f7fb2c84)

## 0.14.1
2020-04-14

### Fixes

- **firebase:** Add firebase account file to container (5cc03fb3e8b9e7f0e4aba679299384b99802e4d8)
- **prod:** Change db timeout (3bc280c4b8b98cb042332d4aedeebd2aba363b2b)

## 0.14.0
2020-04-12

### Features

- **bilan:** Add Surveillance with relation on Physique (2974898111e672bfd21b1cd3938f4f7f519a57b3)

### Fixes

- **date:** Define json date pattern (86161e567680e01502ebfcfa4c9c49712d333e34)

## 0.13.0
2020-04-11

### Features

- **firebaseservice:** Adding Account creation and removal of FirebaseAuth. (5fc3f1ce2214a271fe0da96bfaf001e06cc86c82)

## 0.12.0
2020-04-10

### Features

- **model:** Update victime's models (5a5baefa9f28e3c540e7b6bad1c5adef6d1823ad)

## 0.11.0
2020-04-10

### Features

- **deployment:** Update image on ci (f47916c74c5a6989c7fb0c9a3b34587bf8084602)

## 0.10.1
2020-04-08

### Fixes

- **comments:** Delete unused method (3d9be676bd5d82dd0d20521f5870ecd9edef5aa4)
- **model:** follow (ea09244c95130a40660259852ee935ee6db7e384)

## 0.10.0
2020-04-07

### Features

- Add search method (87cc236510bdb96d32e260489fe49de11c90d747)

## 0.9.2
2020-04-06

### Fixes

- try helping Louis (47d0c25d6c0d61b0b04770c737305c6426f5ee49)

## 0.9.1
2020-04-06

### Fixes

- **personnel:** Add RequestBody (e375b092617631dcebfdfdd4f036d44011edc2c4)

## 0.9.0
2020-04-06

### Features

- **personnel:** Add password (6e2d2886d664780c52f93791c7357b7afb21caf8)

### Fixes

- **personnel:** fix json (70935b87eda71635e93576e4c6c60e292b82f31b)

## 0.8.2
2020-04-05

### Fixes

- **id:** Change generation type (ec9af74569b58c666ffc7ba74e5db3925e8dd0a9)

## 0.8.1
2020-04-05

### Fixes

- **new personnel:** fix all null (18a4c5b10cad22f1488c75195c0884da377fb0ed)

## 0.8.0
2020-04-05

### Features

- **doc:** Add springdoc (58cf85f704b2f6ef6bfeaf7ce67ae0458043f740)

## 0.7.2
2020-04-04

### Fixes

- **project:** refactor some stuff in module (f817f35a783f51472a9612377913827de5e2fe26)

## 0.7.1
2020-04-03

### Fixes

- **ci:** Update ci (d032867c3e054d39edad9c876d5ca96229ae9ec7)

## 0.7.0
2020-04-01

### Features

- **controller:** Switched to JSON data processing, plus full CORS filter. (07411c3cbbeffff157d057c3a6b1aaee281590ea)

### Fixes

- **merge:** Merging fusionned commits (c5f6cc9c81b5f89df24f92f3a1b2e8b0264669d9)

## 0.6.1
2020-03-31

### Fixes

- **deployment:** use latest image (997f10ecf66219f218c04db44fadbae376136f92)

## 0.6.0
2020-03-30

### Features

- Add pagination on Personnel (77a88915a00c309ebfca9a9f328d72e61480ea3a)

### Fixes

- **readme:** Add Pipeline status (7bfc80d37b7efb976bafc67f049203fcd7685f98)
- **model personnel:** Remove idFirebase & password (d405bb018592a8693676b3f5735e248628772c88)
- **models:** Follow modifications from Poste (dcc2cebd724746fb51b9ed1c66ac02dd01fe4df5)
- **poste:** Repar relation with Personnel (907a9c37fe1852902aeb4a03bc3ab7bc858883a3)

## 0.5.1
2020-03-28

### Fixes

- **model:** Put some fields NotNull (2a9337912c4c426a09569c47c93972879cf4715e)

## 0.5.0
2020-03-28

### Features

- **personnel:** Add pagination and sorting && Repair url encoded form error (3055b0ae89935583817a3b3b66653347f2332656)

## 0.4.2
2020-03-24

### Fixes

- **ci:** Change image for version stage (dffb9948b68f644e9d62326f90a4f22eb4ceec58)
- **deployment:** Add namespace in deployment (885b23a314852710d1f5fc6d5f9575d812b2ec8e)

## 0.4.1
2020-03-24

### Fixes

- **ci:** Fixing download link (bd39d5276a6af132260ab6c601d2bb0cb6e9e79d)

## 0.4.0
2020-03-24

### Features

- **ci:** Update Download system and add licence (e0260ac09a46aa1ef46eceea71bb9542db4c798c)

## 0.3.0
2020-03-23

### Features

- **controller:** Adding form possibility in POST and PUT (229d6cd26bf5f67fa9bdbf6bee1eba7c448e467f)

### Fixes

- **model:** Modify model for datasource (8d1db22b23b0d5c0eb520c1112bb96e4ec7d2a29)

## 0.2.1
2020-03-23

### Fixes

- **personnel:** Delete Password && Change url (263387c8425269f474eaa26a01a6278443841c44)

## 0.2.0
2020-03-22

### Features

- **deployment:** Change deploy type from knative to ClusterIP for backend and ingress for the frontend (d0d1196aad1723f91370655b02d6a28d81fd4cb1)

## 0.1.5
2020-03-22

### Fixes

- **container:** Expose server port on container && Add static server port on conf files (019c89125869fc96a545ff1aa05d0276be6db40e)

## 0.1.4
2020-03-22

### Fixes

- **image:** Changing image tag (382d3458217dac8168418b6856c1624a62feaa39)

## 0.1.3
2020-03-22

### Fixes

- **properties:** Change postgresql host (19797fb5aa22b27a7f4366fd327dfdb4a457b449)

## 0.1.2
2020-03-22

### Fixes

- **deployment:** Fix image in deployment (111e43fbed743eaab987870fff4e4c59a29a131a)

## 0.1.1
2020-03-22

### Fixes

- **deployment:** Repair deployment error (42146fe1e282d534addafac1f98fef8d6eafd571)

## 0.1.0
2020-03-22

### Features

- **personnel:** Add first class (7bb757cf5deda8b5b2fac18acb01710429593573)
- **personnel:** Premier test (8d650c0143ae001565fab023df5439bdc070ef52)
- **service:** Add service for spécific method && Update models (bae2bdb411673f842a57a558f3cda66d553cae58)
- **module:** Finish controller && Ready for tests (5e9e343d4cf336ba8326a2096900b86cdc6f9bd1)
- **database:** Migration to postgres (3267426ac796ce6301fd0a254253c3fd3ce51854)
- **ci/cd:** Resolving postgres version issue (23b97104d8b94cfaa50fb0e836f5e10a1cc5d77f)
- **doc:** Adding javadoc (7230c23388ad7223247850742d23c525a1233923)
- update pom.xml (e9883ba6df88197781c79b2ad7bef4a995de701b)
- **dockerfile:** Add Dockerfile (183cb24668bb415bb0b52fe9b55744906f70a935)
- **ci:** Ready for deploy with gitlab ci (9ba2e01c254702364e555ab4247a4d7006794c04)
- **ci:** Ready for deploy with gitlab ci (2c8500d285acac1ad50c8a3a1f5349500ecdc642)
- **ci:** Ready for deploy with gitlab ci *update* (445dd585758cbbf39af067c3050cb920e7316c15)

### Fixes

- **structure:** Delete folders (f7f72abf3b365661e291dbf7a441e44ec2dedb3a)
- **depencies:** Update dep (699e9e64decfc3c16a73242fe763908062928bff)
- **depencies:** remove some dependencies (5d9b37772ac31fdf8e4b6f63bfcab2bd7e84cd4f)
- **properties:** Add hibernate dialect (92a637a95a85d68d897a4989c644e6478a2766d4)
- **ci/cd:** Env variable un properties for auto deploy ci (608843c162cdf8783fd7a179acc6f74bfab1426d)
- **ci:** Correcting package command (1aa2f2a5f57aa63044f587bf537d1bb3e5689106)
- **ci:** repair ci (5529c644168b32ca066ed1e5b8324c1aa2fb6bcf)
- **ci:** try repair docker login in ci (21937d1622483b48a8c84ea9a869f417c5dbd0a9)